#!/bin/bash

docker build -t jaron_website_dev -f Dockerfile_Develop .
docker stop jaron_website_dev
docker rm jaron_website_dev
docker run --name jaron_website_dev -p 8089:80 -v /Users/freese/Programming/website_mk.1/src:/var/www/html -i -t -d jaron_website_dev
