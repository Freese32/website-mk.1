#!/bin/bash

rm -rf www/
mkdir www/
mkdir www/bootstrap-4/
cp -a bootstrap-4/. www/bootstrap-4/
cp -a src/*.jpg www/
cp -a src/*.html www/
cp -a src/*.css www/

docker build -t jaron_website_release -f Dockerfile_Release .
docker stop jaron_website_release
docker rm jaron_website_release
docker run -d --name jaron_website_release -p 8080:80 -i -t jaron_website_release
